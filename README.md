# Semantic Versioning Tagger

## Mission Statement

For a given GitLab project and a given protected branch, the semantic versioning
tagger checks if the last commit belongs to a merge request toward this branch.
If yes, it tags it by increasing the last [semantic
version](https://semver.org/) tag `X.Y.Z` of the branch, using the information
inside the merge request regarding which digit (`X`, `Y`, or `Z`) will be
increased.

## Example

Let's suppose that the protected branch below is `master`.

![Semantic versioning flow](figures/semantic_versioning_flow.png)

We have three cases
 1. If the merge request is labeled `major`
      - The tag of the last (merged) commit will be `2.0.0`.
 2. If the merge request is labeled `minor`
      - The tag of the last commit will be `1.1.0`.
 3. If the merge request is labeled `patch`
      - The tag of the last commit will be `1.0.2`.

## Special Cases

 1. No previous version exists
      - The tag `1.0.0` is given
 2. No information about which digit (`X`, `Y`, or `Z`) will be increased
      - The last digit `Z` is increased by default
 3. We are inside a protected branch _other_ than `master`
      - The version is prefixed with the branch name followed by a dash
      - A possible tag could be `branchname-1.0.2`, where `branchname` is the
        name of the protected branch

## Installation

  + Just add the following code to a `.gitlab-ci.yml` file in the root folder of
    your GitLab project.

    ```yml
    Semantic Versioning Tagger:
      stage: deploy
      image: registry.gitlab.com/semver-tagger/semver-tagger
      script:
        - semver-tagger
    ```

  + You will also need to have a GitLab personal access token.
      - This can be created via User Settings > [Access
        Tokens](https://gitlab.com/profile/personal_access_tokens).
      - Don't forget to check the `api` checkbox.

  + Then go to your project in Settings > CI/CD > Variables.
      - Add the `PERSONAL_ACCESS_TOKEN` variable with the appropriate value.
