#!/usr/bin/env python
'''Automatic repository tagging using Semantic Versioning'''

import fnmatch
import os
import subprocess
import sys

import git
import gitlab
import semantic_version

# Support both Python 2 and 3 urlparse
try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

# Separator between branch name and version
SEPARATOR = '-'


def get_repo_params_from_env_variables():
    '''Environment variables into local variables for repository parameters'''

    # Get current branch (B)
    if os.environ.get('CI_COMMIT_REF_NAME'):
        branch = os.environ.get('CI_COMMIT_REF_NAME')
        latest_commit = os.environ.get('CI_COMMIT_SHA')
    elif os.environ.get('gitlabSourceBranch'):
        branch = os.environ.get('gitlabSourceBranch')
        latest_commit = os.environ.get('GIT_COMMIT')
    else:
        sys.exit('Could not figure out current branch!')

    # Get project's URL
    if os.environ.get('CI_PROJECT_URL'):
        url = os.environ.get('CI_PROJECT_URL')
    elif os.environ.get('gitlabSourceRepoHomepage'):
        url = os.environ.get('gitlabSourceRepoHomepage')
    else:
        sys.exit('Could not get the URL of the project!')

    # Split URL into domain and project name
    url_parsed = urlparse(url)
    gitlab_domain = url_parsed.scheme + '://' + url_parsed.netloc
    # Remove first character (slash) from the path
    assert url_parsed.path[0] == '/'
    project_name = url_parsed.path[1:]

    return branch, latest_commit, gitlab_domain, project_name


def branch_is_protected(branch, remote_project):
    '''Check if branch is protected'''
    is_protected = False
    for protected_branch in remote_project.protectedbranches.list():
        if fnmatch.fnmatch(branch, protected_branch.name):
            is_protected = True
            break
    return is_protected


def tags_for(ref):
    '''Return a list with the tags pointing at ref'''
    output = subprocess.check_output(['git', 'tag', '--points-at', ref])
    return output.decode('utf-8').splitlines()


def get_highest_version(tags, branch):
    '''Return the highest version (if any) in the tags list'''
    highest_semver = None
    for tag in tags:
        current_semver = tag_to_semver(tag, branch)
        if current_semver:
            if not highest_semver or highest_semver < current_semver:
                highest_semver = current_semver
    return highest_semver


def tag_to_semver(tag, branch):
    '''Parse a tag and converts it to a semantic_version object'''
    if branch != 'master':
        # Remove branch prefix if it exists
        if not tag.startswith(branch + SEPARATOR):
            return None
        tag = tag[len(branch + SEPARATOR):]
    try:
        return semantic_version.Version.coerce(tag)
    except ValueError:
        return None


def mine_existing_versions(latest_commit, branch):
    '''Iterate through the previous tags and get the latest existing version'''
    existing_version = None
    previous_tagged_commit = None
    existing_master_version = None
    previous_tagged_commit_master = None
    local_repo = git.Repo('.')
    for commit in local_repo.iter_commits(latest_commit):
        tags = tags_for(commit.hexsha)
        existing_version = get_highest_version(tags, branch)
        if existing_version:
            previous_tagged_commit = commit.hexsha
            break
        if not existing_master_version:
            existing_master_version = get_highest_version(tags, 'master')
            previous_tagged_commit_master = commit.hexsha
    return (existing_version, previous_tagged_commit, existing_master_version,
            previous_tagged_commit_master)


def increase_version(existing_version, branch, latest_commit, remote_project):
    '''Decide the next version considering merge request labels'''

    # Get the labels from merge request M targeted to B
    new_version = None
    merge_request_commit = latest_commit
    merge_request_found = False
    for merge_request in remote_project.commits.get(
            merge_request_commit).merge_requests():
        if merge_request['target_branch'] == branch:
            merge_request_found = True
            break
    if not merge_request_found:
        parents = subprocess.check_output(
            ['git', 'log', '--pretty=%P', '-n', '1',
             latest_commit]).decode('utf-8').split()
        if len(parents) == 2:
            merge_request_commit = parents[1]
    for merge_request in remote_project.commits.get(
            merge_request_commit).merge_requests():
        if merge_request['target_branch'] == branch:
            sys.stderr.write(merge_request['web_url'])
            # Get the labels L set for M
            # Increase tag considering L
            labels = to_lowercase(merge_request['labels'])
            if 'major' in labels:
                sys.stderr.write(' is labeled major\n')
                new_version = existing_version.next_major()
            elif 'minor' in labels:
                sys.stderr.write(' is labeled minor\n')
                new_version = existing_version.next_minor()
            elif 'patch' in labels:
                sys.stderr.write(' is labeled patch\n')
                new_version = existing_version.next_patch()
            else:
                sys.stderr.write(' is not labeled; assuming a patch change\n')
                new_version = existing_version.next_patch()
    if not new_version:
        sys.stderr.write('No merge request found; assuming a patch change\n')
        new_version = existing_version.next_patch()
    return new_version


def to_lowercase(str_list):
    '''Convert the letters of the strings in the given list to lowercase'''
    return [string.lower() for string in str_list]


def create_and_push_tag(remote_project, commit, tag, branch):
    '''Create a tag both locally and remotely'''
    if branch != 'master':
        tag = branch + SEPARATOR + tag
    if tag in tags_for(commit):
        sys.stderr.write('A valid version already exists\n')
        print(tag)
        return
    local_repo = git.Repo('.')
    local_repo.create_tag(tag)
    remote_project.tags.create({'tag_name': tag, 'ref': commit})
    print(tag)


def main():
    '''Main wrapper'''

    # Check environment variables
    if os.environ.get('CI_COMMIT_TAG'):
        sys.stderr.write(
            'semver-tagger is disabled for pipelines triggered by new tags\n')
        return
    branch, latest_commit, gitlab_domain, project_name = (
        get_repo_params_from_env_variables())

    # Connect to GitLab project
    gitlab_handler = gitlab.Gitlab(gitlab_domain,
                                   private_token=PERSONAL_ACCESS_TOKEN)
    remote_project = gitlab_handler.projects.get(project_name)

    if not branch_is_protected(branch, remote_project):
        sys.stderr.write(
            'Will not add version/tag to the non-protected branch ' + branch +
            '!\n')
        return

    # Get tags for latest commit (C) of branch B
    tags = tags_for(latest_commit)

    # If a valid tag already exists
    #   Set the environment variable with this tag, print it, and exit
    existing_version = get_highest_version(tags, branch)
    if existing_version:
        create_and_push_tag(remote_project, latest_commit,
                            str(existing_version), branch)
        return

    (existing_version, previous_tagged_commit, existing_master_version,
     previous_tagged_commit_master) = mine_existing_versions(
         latest_commit, branch)

    # If branch-specific version doesn't exist, consider master version
    if not existing_version:
        if existing_master_version:
            existing_version = existing_master_version
            previous_tagged_commit = previous_tagged_commit_master

    # If a valid tag exists for a previous commit
    if existing_version:
        if previous_tagged_commit == latest_commit:
            new_version = existing_version
        else:
            # Version should be increased
            new_version = increase_version(existing_version, branch,
                                           latest_commit, remote_project)
    else:
        # No valid previous tag exist
        # Set default tag for C
        new_version = semantic_version.Version('1.0.0')

    # Set the environment variable with this tag, print it, and exit
    create_and_push_tag(remote_project, latest_commit, str(new_version),
                        branch)


if __name__ == '__main__':
    if os.environ.get('PERSONAL_ACCESS_TOKEN'):
        PERSONAL_ACCESS_TOKEN = os.environ.get('PERSONAL_ACCESS_TOKEN')
    else:
        sys.exit('Please add your PERSONAL_ACCESS_TOKEN as a CI/CD variable!')
    main()
