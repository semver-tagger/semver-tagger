FROM python:3.7-alpine

RUN apk add --no-cache git
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt && \
    rm /usr/src/app/requirements.txt
COPY semver_tagger.py /usr/src/app/
RUN ln -s /usr/src/app/semver_tagger.py /usr/local/bin/semver-tagger

CMD ["semver-tagger"]
