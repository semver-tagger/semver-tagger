#!/usr/bin/env python
'''Clean up created tags from previous test cases runs'''

import os
import sys

import gitlab


def delete_tags_from_project(gitlab_handler, project, tags):
    '''Delete given tags from the project'''
    test_project = gitlab_handler.projects.get(project)
    for created_tag in tags:
        try:
            test_project.tags.delete(created_tag)
        except gitlab.exceptions.GitlabDeleteError:
            sys.stderr.write('Could not delete ' + created_tag + '\n')


def main():
    '''Main wrapper'''
    personal_access_token = os.environ['PERSONAL_ACCESS_TOKEN']

    # private token or personal token authentication
    gitlab_handler = gitlab.Gitlab('https://gitlab.com',
                                   private_token=personal_access_token)

    delete_tags_from_project(
        gitlab_handler, 'semver-tagger/semver-tagger-project-for-testing-1', [
            '2.0.0', 'branch1-2.0.0', 'branch2-1.1.0', 'branch3-1.0.2',
            'branch4-1.0.2', 'branch5-1.0.2', 'branch6-2.0.0', 'branch7-1.2.0',
            'branch8-1.1.1', 'branch9-1.1.1', 'branch10-1.1.1',
            'branch11-1.0.0'
        ])

    delete_tags_from_project(
        gitlab_handler, 'semver-tagger/semver-tagger-project-for-testing-2', [
            'branch20-1.1.1', 'branch21-1.0.1', 'branch22-1.5.1',
            'branch23-1.0.1'
        ])


if __name__ == '__main__':
    main()
