#!/bin/sh
set -e

PROJECT=1
# An empty GitLab project should have been created named
# Semantic Versioning Tagger - Project for Testing 1
git clone git@gitlab.com:semver-tagger/semver-tagger-project-for-testing-$PROJECT.git
cd semver-tagger-project-for-testing-$PROJECT
echo "# [Semantic Versioning Tagger](https://gitlab.com/semver-tagger/semver-tagger) - Project for Testing $PROJECT" > README.md
git add README.md
git commit -m "Describe project"
cp ../.gitlab-ci.yml .
git add .gitlab-ci.yml
git commit -m "Enable Semantic Versioning Tagger"
git tag bar-4.0.0

git checkout -b branch11 master
git commit -m "Test commit T" --allow-empty
git checkout -b source_branch11
git commit -m "Test commit U" --allow-empty
# Manually create a merge request to branch11

git checkout master
git commit -m "Test commit A" --allow-empty
git tag 1.0.0
git tag foo
git commit -m "Test commit B" --allow-empty
git tag 1.0.1
git tag bla-3.0.0

git checkout -b source_branch0 master
git commit -m "Test commit C" --allow-empty
# Manually create a merge request to master with "major" label

git checkout -b branch1 master
git checkout -b source_branch1 master
git commit -m "Test commit D" --allow-empty
# Manually create a merge request to branch1 with "major" label

git checkout -b branch2 master
git checkout -b source_branch2 master
git commit -m "Test commit E" --allow-empty
# Manually create a merge request to branch2 with "minor" label

git checkout -b branch3 master
git checkout -b source_branch3 master
git commit -m "Test commit F" --allow-empty
# Manually create a merge request to branch3 with "patch" label

git checkout -b branch4 master
git checkout -b source_branch4 master
git commit -m "Test commit G" --allow-empty
# Manually create a merge request to branch4 without any label

git checkout -b branch5 master
git commit -m "Test commit H" --allow-empty
# Don't create a merge request

git checkout -b unprotected_branch master
git checkout -b source_unprotected_branch master
git commit -m "Test commit I" --allow-empty
# Manually create a merge request to unprotected_branch with "major" label

git checkout -b branch6 master
git commit -m "Test commit J" --allow-empty
git tag branch6-1.1.0
git checkout -b source_branch6
git commit -m "Test commit K" --allow-empty
# Manually create a merge request to branch6 with "major" label

git checkout -b branch7 master
git commit -m "Test commit L" --allow-empty
git tag branch7-1.1.0
git checkout -b source_branch7
git commit -m "Test commit M" --allow-empty
# Manually create a merge request to branch7 with "minor" label

git checkout -b branch8 master
git commit -m "Test commit N" --allow-empty
git tag branch8-1.1.0
git checkout -b source_branch8
git commit -m "Test commit O" --allow-empty
# Manually create a merge request to branch8 with "patch" label

git checkout -b branch9 master
git commit -m "Test commit P" --allow-empty
git tag branch9-1.1.0
git checkout -b source_branch9
git commit -m "Test commit Q" --allow-empty
# Manually create a merge request to branch9 without any label

git checkout -b branch10 master
git commit -m "Test commit R" --allow-empty
git tag branch10-1.1.0
git commit -m "Test commit S" --allow-empty
# Don't create a merge request

# Don't forget to protect the appropriate branches!
git push -u origin --all
git push -u origin --tags
