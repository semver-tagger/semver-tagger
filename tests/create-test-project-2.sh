#!/bin/sh
set -e

PROJECT=2
# An empty GitLab project should have been created named
# Semantic Versioning Tagger - Project for Testing 2
git clone git@gitlab.com:semver-tagger/semver-tagger-project-for-testing-$PROJECT.git
cd semver-tagger-project-for-testing-$PROJECT
echo "# [Semantic Versioning Tagger](https://gitlab.com/semver-tagger/semver-tagger) - Project for Testing $PROJECT" > README.md
git add README.md
git commit -m "Describe project"
cp ../.gitlab-ci.yml .
git add .gitlab-ci.yml
git commit -m "Enable Semantic Versioning Tagger"
git tag branch23-1.0.0

git commit -m "Test commit A" --allow-empty
git tag 1.0.0

git commit -m "Test commit B" --allow-empty
git tag 1.0.1

git checkout -b branch21 master

git checkout master
git commit -m "Test commit C" --allow-empty
git tag 1.1.0

git checkout -b branch20 master
git commit -m "Test commit D" --allow-empty

git checkout master
git commit -m "Test commit E" --allow-empty
git commit -m "Test commit F" --allow-empty
git tag 2.2.0
git tag branch22-1.5.0

git checkout -b branch22 master
git commit -m "Test commit G" --allow-empty

git checkout master
git commit -m "Test commit H" --allow-empty
git tag 3.0.0

git checkout -b branch23 master
git commit -m "Test commit I" --allow-empty

git push -u origin --all
git push -u origin --tags
