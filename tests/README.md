# Semantic Versioning Tagger Test Cases

## 1 Previous versions derive from `master` branch

  + The following figure applies to merge requests toward a protected branch.
  + Previous versions/tags don't have any prefix as they come from `master`.

![Merging to master branch versions](figures/master_tags.png)

### 1.1 Merging to `master` a `major` modification

  + The above purple branch is `master`.
  + The new tag is expected to be `2.0.0`.

### 1.2 Merging to a protected branch a `major` modification

  + The above purple branch is named `branch1`.
  + The new tag is expected to be `branch1-2.0.0`.

### 1.3 Merging to a protected branch a `minor` modification

  + The above purple branch is named `branch2`.
  + The new tag is expected to be `branch2-1.1.0`.

### 1.4 Merging to a protected branch a `patch` modification

  + The above purple branch is named `branch3`.
  + The new tag is expected to be `branch3-1.0.2`.

### 1.5 Merging to a protected branch without a label

  + The above purple branch is named `branch4`.
  + The new tag is expected to be `branch4-1.0.2`.

### 1.6 Pushing to a protected branch without a merge request

  + The above purple branch is named `branch5`.
  + The new tag is expected to be `branch5-1.0.2`.

### 1.7 Pushing to an unprotected branch

  + No new version should be given.

### 1.8 Repeat test cases when irrelevant tags also exist

  + Add the `bla-3.0.0` tag together with the `1.0.1` tag.
  + Add the `foo` tag together with the `1.0.0` tag.
  + Add the `bar-4.0.0` tag below the `1.0.0` tag.
  + Repeat above test cases.

## 2 Previous versions derive from a non-`master` protected branch

![Merging to protected branch versions](figures/branch_tags.png)

### 2.1 Merging to a protected branch a `major` modification

  + The above purple branch (`branchname`) is named `branch6`.
  + The new tag is expected to be `branch6-2.0.0`.

### 2.2 Merging to a protected branch a `minor` modification

  + The above purple branch is named `branch7`.
  + The new tag is expected to be `branch7-1.2.0`.

### 2.3 Merging to a protected branch a `patch` modification

  + The above purple branch is named `branch8`.
  + The new tag is expected to be `branch8-1.1.1`.

### 2.4 Merging to a protected branch without a label

  + The above purple branch is named `branch9`.
  + The new tag is expected to be `branch9-1.1.1`.

### 2.5 Pushing to a protected branch without a merge request

  + The above purple branch is named `branch10`.
  + The new tag is expected to be `branch10-1.1.1`.

## 3 No previous versions exist

![No previous versions exist](figures/no_previous_versions_exist.png)

  + The above purple branch is named `branch11`.
  + The expected new tag is `branch11-1.0.0`.
  + This is independent of the merge request label set.

## 4 Mixed tags from `master` and protected branches

For this test category we will use a different test repository.

![Mixed tags from master and protected branches](figures/mixed_tags.png)

### 4.1 Execute pipeline for `master` branch when tag already exists

  + The tag should remain `3.0.0`.

### 4.2 Execute pipeline for protected branch when tag already exists

  + The tag should be `branch20-1.1.1`.

### 4.3 Execute pipeline for a new protected branch using a tagged commit

  + The new protected branch name will be `branch21`.
  + The tag should be `branch21-1.0.1`.

### 4.4 Create branch from a commit with two tags

  + The new branch is named `branch22`.
  + The new tag is expected to be `branch22-1.5.1`.

### 4.5 Execute pipeline for a new protected branch using a tagged commit

  + The new branch is named `branch23`.
  + The new tag is expected to be `branch23-1.0.1`.
  + It will succeed `branch23-1.0.0`.
